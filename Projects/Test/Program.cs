﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;

namespace Test
{
    class Program
    {
        static void Main()
        {
            Console.ForegroundColor = ConsoleColor.Yellow;

            var progs = Directory.GetFiles(Environment.CurrentDirectory);
            progs = progs.Select(Path.GetFileName).ToArray();
            progs = progs.Where(f => f.EndsWith(".exe")).ToArray();
            progs = progs.Where(f => !f.Contains(Path.GetFileName(Process.GetCurrentProcess().ProcessName.Replace("vshost","")))).ToArray();

            var procs = progs.Select(prog =>
            {
                var p = new Process
                {
                    StartInfo =
                    {
                        FileName = prog,
                        UseShellExecute = false,
                        CreateNoWindow = true,
                        RedirectStandardOutput = true
                    }
                };
                return p;
            }).ToList();

            procs.ForEach(p =>
            {
                p.Start();
                p.WaitForExit();
                Console.WriteLine(p.StandardOutput.ReadToEnd());
                p.WaitForExit();
            });
        }
    }
}
