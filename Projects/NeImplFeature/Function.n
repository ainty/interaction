﻿using Nemerle.Collections;
using Nemerle.Text;
using Nemerle.Utility;

using System;
using System.Collections.Generic;
using System.Linq;

namespace NeImplFeature
{
    public class Function
    {
        public static Calculate() : int
        {
            def sum(lst)
            {
              match (lst)
              {
                | head :: tail => head + sum(tail)
                | [] => 0
              }
            }
            
            sum($[0 .. 999]);
        }
    }
}
