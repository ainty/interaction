﻿namespace FsImplFeature

type Function() = 
    static member Calculate() = Array.sum [| for i in 1 .. 999 -> i |]
