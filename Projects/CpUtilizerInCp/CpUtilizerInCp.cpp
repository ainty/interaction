// CsUtilizerInCp.cpp : main project file.

#include "stdafx.h"

using namespace System;
using namespace System::Diagnostics;
using namespace CpImplFeature;

int main()
{
	Stopwatch^ sw = Stopwatch::StartNew();

    for (int i = 0; i < 1000; i++)
    {
        Function::Calculate();
    }

    sw->Stop();
    Console::WriteLine("C++ code called from C++ in {0}", sw->Elapsed.TotalMilliseconds);
    return 0;
}
