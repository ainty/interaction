﻿using System;
using System.Console;
using System.Diagnostics;
using NeImplFeature;

module Program
{
   Main() : void
    {
        def sw = Stopwatch.StartNew();
        
        def _ = Function.Calculate();
        
        sw.Stop();
        WriteLine("Nemerle code called from Nemerle in {0}", sw.Elapsed.TotalMilliseconds);
    }
}