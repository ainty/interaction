﻿open System.Diagnostics
open FsImplFeature

[<EntryPoint>]
let main argv = 
    let sw = Stopwatch.StartNew()
    let t = Function.Calculate()
    sw.Stop()
    printfn "F# code called from F# in %f" sw.Elapsed.TotalMilliseconds
    0 // return an integer exit code