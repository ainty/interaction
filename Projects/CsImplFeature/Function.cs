﻿namespace CsImplFeature
{
    public class Function
    {
        public static int Calculate()
        {
            var v = 0;

            for (var i = 0; i < 1000; i++)
            {
                v += i;
            }

            return v;
        }
    }
}
