﻿using System;
using System.Console;
using System.Diagnostics;
using CsImplFeature;

module Program
{
    Main() : void
    {
        def sw = Stopwatch.StartNew();
        
        def _ = Function.Calculate();
        
        sw.Stop();
        WriteLine("C# code called from Nemerle in {0}", sw.Elapsed.TotalMilliseconds);
    }
}