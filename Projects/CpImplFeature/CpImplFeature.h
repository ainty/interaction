// CpImplFeature.h

#pragma once

using namespace System;

namespace CpImplFeature {

	public ref class Function
	{
		public:

		static int Calculate();
	};
}
