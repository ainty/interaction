﻿using System;
using System.Diagnostics;
using NeImplFeature;

namespace NeUtilizerInCs
{
    class Program
    {
        static void Main()
        {
            var sw = Stopwatch.StartNew();

            for (var i = 0; i < 1000; i++)
            {
                Function.Calculate();
            }

            sw.Stop();
            Console.WriteLine("Nemerle code called from C# in {0}", sw.Elapsed.TotalMilliseconds);
        }
    }
}
