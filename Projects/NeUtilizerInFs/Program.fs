﻿// Learn more about F# at http://fsharp.net
// See the 'F# Tutorial' project for more help.
open System
open System.Diagnostics
open NeImplFeature

[<EntryPoint>]
let main argv = 
    let sw = Stopwatch.StartNew()
    let t = Function.Calculate()
    sw.Stop()
    printfn "Nemerle code called from F# in %f" sw.Elapsed.TotalMilliseconds
    0
