﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTests
{
    [TestClass]
    public class InteractionTests
    {
        [TestMethod]
        public void TestCall()
        {
            var a = CpImplFeature.Function.Calculate();
            var b = CsImplFeature.Function.Calculate();
            var c = FsImplFeature.Function.Calculate();
            var d = NeImplFeature.Function.Calculate();

            Assert.AreEqual(a, b);
            Assert.AreEqual(b, c);
            Assert.AreEqual(c, d);
        }
    }
}
