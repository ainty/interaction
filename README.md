### What is this repository for? ###

* Time measurement of interaction between different platforms in some cases

### How do I get set up? ###

* Install nemerle from http://nemerle.org
* Download sources from repo
* Build
* Run Test.exe
* Observe generated report

### Possible report output

![InteractionMeasurementReport.png](https://bitbucket.org/repo/Ep9RLx/images/2471449146-InteractionMeasurementReport.png)